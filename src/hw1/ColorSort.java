package hw1;

import java.util.Arrays;
import java.util.Random;

public class ColorSort {

   enum Color {
	   red, green, blue
	   }
   
   public static void main (String[] args) {
      // for debugging
	   Color[] balls = new Color[100000];
	   
	   Color[] values = Color.values();
       Random rand = new Random();
       for (int i = 0; i < balls.length; i++)
           balls[i] = values[rand.nextInt(values.length)];
       
       System.out.println("algus: " + Arrays.toString(balls));
       
       reorder(balls);

       System.out.println("p�rast: " + Arrays.toString(balls));
   }
   public static void reorder (Color[] balls) {
      // TODO!!! Your program here
	   int vasak = 0; 
	   int keskel = 0; 
	   int parem = balls.length - 1;
	   
	   while (keskel <= parem) 
	            switch (balls[keskel]) {
	                case red:
	                    swap(balls, vasak++, keskel++);
	                    break;
	                case green:
	                    keskel++;
	                    break;
	                case blue:
	                    swap(balls, keskel, parem--);
	                    break;
	            }
	    }
	 
	    private static void swap(Color[] arr, int a, int b) {
	        Color tmp = arr[a];
	        arr[a] = arr[b];
	        arr[b] = tmp;
	    }
	}

// Ref : http://www.makeinjava.com/dutch-national-flag-problem-sort-array-containing-0s-1s-2s/
// Ref : https://rosettacode.org/wiki/Dutch_national_flag_problem#Java
// Ref : https://en.wikipedia.org/wiki/Dutch_national_flag_problem